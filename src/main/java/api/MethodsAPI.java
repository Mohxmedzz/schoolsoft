package api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Path("methods")
public class MethodsAPI {

    @GET
    @Path("userswithreversednames")
    public static Response usersWithReversedNames() throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        StringBuilder stringBuilder;
        List<User> userList;
        userList = objectMapper.readValue(
                new File("src/main/res/users.json"),
                new TypeReference<List<User>>() {
                }
        );
        List<String> reversedNames = new ArrayList<>();
        for (User user : userList) {
            stringBuilder = new StringBuilder(user.getFirstname()).reverse();
            reversedNames.add(String.valueOf(stringBuilder));
        }
        return Response.ok(
                reversedNames.toString()
        ).build();
    }

    static boolean isItPalindrome(String str) {
        int i = 0, j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
/**
 * To try it out hold ctrl and left-click the following link
 * <a href="http://localhost:63342/coding-exercise-main/methods/src/main/res/html/Palindrome.html?_ijt=brsvmkriiqqiko8l4o76m9p04a&_ij_reload=RELOAD_ON_SAVE">...</a>
 * */
    @POST
    @Path("ispalindrome")
    public static Response isPalindrome(@FormParam("string") String string) {
        string = string.toLowerCase(Locale.ROOT);
        String check;
        if (isItPalindrome(string)) {
            check = " is a palindrome";
        } else {
            check = " is not a palindrome";
        }
        return Response.ok(
                string + check
        ).build();
    }
/**
 * To try it out hold ctrl and left-click the following link
 * <a href="http://localhost:63342/coding-exercise-main/methods/src/main/res/html/PadNumberWithZeroes.html?_ijt=brsvmkriiqqiko8l4o76m9p04a&_ij_reload=RELOAD_ON_SAVE">...</a>
 * */
    @POST
    @Path("padnumberwithzeroes")
    public static Response padNumberWithZeroes(@FormParam("number") int number) {
        String fiveDigit = String.format("%05d", number);
        return Response.ok(fiveDigit
        ).build();
    }
/**
 * To try it out hold ctrl and left-click the following link
 * <a href="http://localhost:63342/coding-exercise-main/methods/src/main/res/html/FindNTheLargestNumber.html?_ijt=brsvmkriiqqiko8l4o76m9p04a&_ij_reload=RELOAD_ON_SAVE">...</a>
 * */
    @POST
    @Path("findnthlargestnumber")
    public static Response findNthLargestNumber(@FormParam("numbers") List<Integer> numbers) {
        int nthLargestNumber = numbers.get(0);
        for (Integer number : numbers) {
            if (number > nthLargestNumber)
                nthLargestNumber = number;
        }
        return Response.ok(
                nthLargestNumber
        ).build();
    }

}
