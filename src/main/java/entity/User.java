package entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    int id;
    String firstname;
    String lastname;
    String username;
    int age;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", username='" + username + '\'' +
                ", age=" + age +
                '}';
    }
}
